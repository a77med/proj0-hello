# Proj0-Hello
-------------
Author: Ahmed Al Ali

Contact Address: aalali@uoregon.edu

Description: This is a simple hello world project that prints out "Hello world" as soon as the makefile is run. Mainly to understand a bit of how the submission is supposed to happen.
